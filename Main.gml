 #define Main
  // Enums
  enum AxeState {
    Fly,
    Hover,
    Grounded,
    CallBack,
    Count
  }

  // Gear Category
  globalvar GearThrowingAxe;
  GearThrowingAxe = GearCategoryCreate(undefined, "Throwing Axes", true);

  // Extra sprites
  globalvar sprBansheeBaneGhost;
  sprBansheeBaneGhost = sprite_add("Resources/sprBansheeBaneGhost.png", 1, false, false, 14, 12);
  
  test = ItemCreate(undefined, "test", "test", sprEmpty, 0, 0, 0, 0, 0, [Item.Wood, 1]);
  
  // Items
  globalvar ItemObsidianAxe, ItemNordicSteelAxe, ItemAncientAxe, ItemStormBringer, ItemCataclysm, ItemDiabella, ItemBansheeBane;
  ItemObsidianAxe = AxeCreate(
    "Obsidian Axe",
    "Hurl at your enemies, but be sure to pick it back up!",
    "sprObsidianAxe.png",
    500,
    [Item.Obsidian, 50, Item.RoyalSteel, 5, Item.Wood, 10],
    3
  );
  ItemNordicSteelAxe = AxeCreate(
    "Nordic Steel Axe",
    "Strong enough to crash through resources!",
    "sprNordicSteelAxe.png",
    1500,
    [Item.CosmicSteel, 20, Item.VoidSteel, 20, Item.Wood, 15],
    8
  );
  ItemAncientAxe = AxeCreate(
    "Ancient Axe",
    "Once owned by a great warrior, this axe will return to you when called!",
    "sprAncientAxe.png",
    5000,
    [Item.VoidRose, 10, Item.VoidSteel, 30, Item.CosmicSteel, 20, Item.Wood, 20],
    15
  );
  ItemStormBringer = AxeCreate(
    "Storm Bringer",
    "Radiating with energy, this axe will electocute all in its path",
    "sprStormBringer.png",
    8000,
    [Item.StarFragment, 50, Item.CosmicSteel, 50, Item.Wood, 40],
    25
  );
  ItemCataclysm = AxeCreate(
    "Cataclysm",
    "Constantly smoldering, this axe will explode before appearing back in your hand",
    "sprCataclysm.png",
    100000,
    [Item.Cinderbloom, 150, Item.CosmicSteel, 200, Item.Wood, 50],
    35
  );
  ItemBansheeBane = AxeCreate(
    "Banshee Bane",
    "Haunted by a spirit long forgotten, this axe will fire clones of itself around you.",
    "sprBansheeBane.png",
    800000,
    [Item.LegendaryGem, 100, Item.CosmicSteel, 300, Item.VoidSteel, 300, Item.Wood, 100],
    65
  );
  ItemDiabella = AxeCreate(
    "Diabella",
    "Fallen from the heavens, this axe will steer itself towards all targets before returning to your hand",
    "sprDiabella.png",
    500000,
    [Item.CosmicSteel, 500, Item.VoidSteel, 500, Item.Uranium, 500, Item.Wood, 150],
    50
  );

#define AxeCreate(name, description, spriteFileName, value, recipe, damage)
  var _sprite = sprite_add("Resources/" + spriteFileName, 1, false, false, 0, 0);
  sprite_set_offset(_sprite, sprite_get_width(_sprite) / 2, sprite_get_height(_sprite) / 2);
  var _axe = ItemCreate(
    undefined,
    name,
    description,
    _sprite,
    ItemType.Gear,
    ItemSubType.None,
    value,
    0,
    0,
    recipe,
    ScriptWrap(UseThrowingAxe),
    undefined,
    true,
    damage
  );
  GearCategoryAddItems(GearThrowingAxe, _axe);
  return _axe;

#define UseThrowingAxe(item)
  if (item != ItemBansheeBane) {
    var _axe = AxeSpawn(objPlayer.x, objPlayer.y, item);
    if (item == ItemObsidianAxe) _axe.mask_index = sprObsidianAxeMask;
    if (item == ItemDiabella) {
      GlowOrderCreate(_axe, 0.3, c_white);
      _axe.targetList = ds_list_create();
      collision_circle_list(mouse_x, mouse_y, 100, all, false, true, _axe.targetList, true);
    }
  } else {
    var i = 0;
    repeat (7) {
      var _axe = AxeSpawn(objPlayer.x, objPlayer.y, item);
      with (_axe) {
        sprite_index = sprBansheeBaneGhost;
        timer = 120;
        dir = (360 / 7) * i++;
        targetX = x + lengthdir_x(100, dir);
        targetY = y + lengthdir_y(100, dir);
        GlowOrderCreate(_axe, 0.3, c_white);
        trailSize = 7;
      }
    }
  }

#define AxeSpawn(x, y, axe)
  var _axe = ModObjectSpawn(x, y, objPlayer.depth);
  with (_axe) {
    sprite_index = ItemGet(axe, ItemData.Sprite);
    dir = point_direction(x, y, mouse_x, mouse_y);
    item = axe;
    state = AxeState.Fly;
    targetX = mouse_x;
    targetY = mouse_y;
    target = noone;
    targetList = noone;
    timer = 0;
    lifeTimer = 0;
    trailList = ds_list_create();;
    trailSize = 0;
    GearEdit(GearThrowingAxe, GearData.Current, noone);
    ToolbarRefresh();
    InstanceAssignMethod(id, "Step", ScriptWrap(AxeUpdate));
    InstanceAssignMethod(id, "Draw", ScriptWrap(AxeDraw), false);
    InstanceAssignMethod(id, "Destroy", ScriptWrap(AxeDestroy));
  }
  return _axe;
  
#define AxeUpdate

  // Life timer
  lifeTimer++;

  // State machine
  switch (state) {
    case AxeState.CallBack:
    case AxeState.Fly:
      // Axe of the Goddess
      if (item == ItemDiabella) {

        // Find new target if needed
        while (target == noone || !instance_exists(target)) {
          target = ds_list_find_value(targetList, 0);

          // If no more targets, return to the player
          if (target == undefined) {
            target = noone;
            state = AxeState.CallBack;
            break;
          }

          // Remove this target from the list
          ds_list_delete(targetList, 0);

          // If not eligable to hit, try again
          if (!instance_exists(target) || !CanHit(target) || object_is_ancestor(target.object_index, parSpecialObject)) {
            target = noone;
            continue;
          }
        }
      }

      // CallBack specific
      if (state == AxeState.CallBack) {
        image_angle -= 30;
        target = objPlayer;
      } else {
        image_angle += 30;
      }

      // Track instance (if one exists)
      if (instance_exists(target)) {
        // Set the coordinates
        targetX = target.x;
        targetY = target.y;
      }

      // Move and collide
      dir = point_direction(x, y, targetX, targetY);
      var _xDelta = lengthdir_x(6, dir);
      var _yDelta = lengthdir_y(6, dir);
      var _inst = instance_place(x + _xDelta, y, parEnemy);
      if (_inst == noone) _inst = instance_place(x, y + _yDelta, parEnemy);
      if (_inst == noone) _inst = instance_place(x + _xDelta, y, parNatural);
      if (_inst == noone) _inst = instance_place(x, y + _yDelta, parNatural);
      if (_inst == noone) _inst = instance_place(x + _xDelta, y, parHerb);
      if (_inst == noone) _inst = instance_place(x, y + _yDelta, parHerb);
      if (_inst && !(object_is_ancestor(_inst.object_index, parSpecialObject))) {
        if (item == ItemObsidianAxe) {
          _xDelta = 0;
          _yDelta = 0;
          state = AxeState.Grounded;
        } else {
          var _dmgOld = objPlayer.dmg;
          objPlayer.dmg = ItemGet(item, ItemData.Damage);
          HitObject(_inst);
          objPlayer.dmg = _dmgOld;
        }
      }
      x += _xDelta;
      y += _yDelta;

      if (point_distance(x, y, targetX, targetY) < 10) {
        if (state == AxeState.CallBack) {
          instance_destroy(id);
          exit;
        }
        if (item == ItemBansheeBane) state = AxeState.Hover;
        else if (item != ItemDiabella) state = AxeState.Grounded;
      }
      break;
    case AxeState.Grounded:
      // Pickup
      if (place_meeting(x, y, objPlayer)) {
        instance_destroy(id);
        exit;
      }

      // Input
      if ((item == ItemAncientAxe || item == ItemStormBringer || item == ItemCataclysm)) {
        if (mouse_check_button_pressed(mb_right)) {
          if (item == ItemCataclysm) {
            ExplosionCreate(x, y, 64, false);
            instance_destroy(id);
            exit;
          }
          state = AxeState.CallBack;
          image_blend = c_blue;
        }
      }
      break;
    case AxeState.Hover:
      timer--;
      image_angle += 2;
      if (timer <= 0) {
        state = AxeState.CallBack;
      }
      break;
  }
  
  // Zapping
  if (item = ItemStormBringer) {
    if (irandom_range(0,100) <= 30) {
      ZapSpawn();
    }
  }

  // Ghost particles
  if (item == ItemBansheeBane) {
    FireEffect($ff9736, $b0ff36);
  }


#define AxeDestroy
  GearEdit(GearThrowingAxe, GearData.Current, item);
  ToolbarRefresh();
  ToolbarSelect(item);

#define AxeDraw

  // Primary render
  draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, image_yscale, image_angle, image_blend, image_alpha);

  // Trail
  if (trailSize != 0) {
    if (lifeTimer % 5 == 0) {
      if (ds_list_size(trailList) == trailSize) {
        ds_list_delete(trailList, 0);
      }
      ds_list_add(trailList, [x, y, image_angle]);
    }
    for (var i = 0; i < ds_list_size(trailList); i++) {
      var _trail = trailList[| i];
      draw_sprite_ext(sprite_index, image_index, _trail[0], _trail[1], image_xscale, image_yscale, _trail[2], image_blend, (i / trailSize) / 1.5);
    }
  }
    