# Throwing Axes

An official mod for Forager demonstrating the gear system in depth. Adds a handful of different throwing axes with unique behavior. This repository is MIT license -- please use it to learn how to make your own mods how you see fit!